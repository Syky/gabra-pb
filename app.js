import { initializeApp } from "https://www.gstatic.com/firebasejs/9.0.2/firebase-app.js";
import { getFirestore, collection, doc, addDoc, getDoc, setDoc, query, where, getDocs } from 'https://www.gstatic.com/firebasejs/9.0.2/firebase-firestore.js';

// Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBDZR8m5kC_3VXyfQqduIhLzN4juecHF7c",
  authDomain: "gabrapb-a1586.firebaseapp.com",
  projectId: "gabrapb-a1586",
  storageBucket: "gabrapb-a1586.appspot.com",
  messagingSenderId: "1102790043",
  appId: "1:1102790043:web:08adeae55ae2cdd9c6f53e",
  measurementId: "G-DEG9FZ8NN6"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

// jQuery
let selectedDate = new Date().toLocaleDateString().replace(/\s+/g, '')
setButtons(selectedDate)
setDatepicker()
setScrolling()

function setDatepicker() {
  var unavailableDates = ["21-1-2022", "26-1-2022", "27-1-2022"];

  function unavailable(date) {
    let dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
    //console.log("DMY " + dmy);
    if ($.inArray(dmy, unavailableDates) == -1) {
        return [true, ""];
    } else {
        return [false, "", "Unavailable"];
    }
  }

  $("#datepicker").datepicker({
    minDate: 0,
    maxDate: "+1M +10D",
    beforeShowDay: unavailable,

    onSelect: function () {
      selectedDate = $(this).datepicker("getDate").toLocaleDateString().replace(/\s+/g, '')
      setButtons(selectedDate)
    }
  });
}

async function setButtons(date) {
  console.log(date);
  const docRef = doc(db, "dates", date);
  const docSnap = await getDoc(docRef);

  const q = query(collection(db, "dates"), where("10", "==", false), where("16", "==", false));

  const querySnapshot = await getDocs(q);
  querySnapshot.forEach((doc) => {
    // doc.data() is never undefined for query doc snapshots
    console.log(doc.id, " => ", doc.data());
  });

  if (docSnap.exists()) {
    const times = docSnap.data();
    console.log(times)

    $.each(times, function (key, value) {
      if (value) {
        $('#but' + key).removeClass('button-red');
        $('#but' + key).addClass('button');
        $('#but' + key).prop("disabled", false);
      } else {
        $('#but' + key).removeClass('button');
        $('#but' + key).addClass('button-red');
        $('#but' + key).prop("disabled", true);
      }
    });
  } else {
    console.log("Neexistuje");
    $('.buttons-wrapper').children('button').each(function () {
      if ($(this).hasClass('button-red')) {
        $(this).removeClass('button-red')
      }

      if (!$(this).hasClass('button')) {
        $(this).addClass('button');
        $(this).prop("disabled", false);
      }
    });
  }
}

function setScrolling() {
  $('a').click(function () {
    $('html, body').animate({
      scrollTop: $($(this).attr('href')).offset().top
    }, 1000);
  });
}

/* scroll animation */
const observer = new IntersectionObserver((entries) => {
  entries.forEach((entry) => {
    if (entry.isIntersecting) {
      entry.target.classList.add('show');
    } else {
      //entry.target.classList.remove('show');
    }
  }) 
}, {rootMargin: '0px 0px 0px 0px'})

const animationElements = document.querySelectorAll('.scroll-animation');
animationElements.forEach(e => observer.observe(e));

/* h1 change */
$('h1').click(function () {
  $('h1').toggleClass('different-h1')
})

// Navbar link active when scrolling through its section
$(document).ready(function () {
  var sectionIds = $('a.nav-link');
  $(document).scroll(function () {
    sectionIds.each(function () {

      var container = $(this).attr('href');
      var containerOffset = $(container).offset().top;
      var containerHeight = $(container).outerHeight();
      var containerBottom = containerOffset + containerHeight;
      var scrollPosition = $(document).scrollTop();

      if (scrollPosition < containerBottom - 40 && scrollPosition >= containerOffset - 40) {
        $(this).addClass('active');
      } else {
        $(this).removeClass('active');
      }

    });
  });
});

/* Navbar  */
$(".hamburger").click(function () {
  $('#logo').toggleClass('open');
  $('.navbar').toggleClass('open', 750);
  $('.navbar-nav').toggleClass('open');
  $('.nav-item').toggleClass("open");
  $('.line:nth-last-child(1)').toggleClass('rotatePlus');
  $('.line:nth-last-child(2)').toggleClass('dissapear');
  $('.line:nth-last-child(3)').toggleClass('rotateMinus');
});

/* Modal */
// Get the <span> element that closes the modal
let modal = document.getElementById("myModal");
let body = document.querySelector('body');
let buttons = document.getElementsByClassName("buttons-wrapper");
let modalPhoto = document.getElementById('opened-photo');
let modalForm = document.getElementsByClassName('modal-content')[0];

buttons = buttons[0].querySelectorAll(':scope > .button');
buttons.forEach(btn => {
  btn.addEventListener('click', (event) => {
    modal.style.display = 'block';
    modalForm.style.display = 'block';
    modalPhoto.style.display = 'none';
    body.style.overflow = 'hidden';

    document.getElementById("modal-date").innerHTML = 'Datum: ' + selectedDate + '<br/>' + 'Čas: ' + btn.value;
  })
});

// When the user clicks on <span> (x), close the modal
$('.close').click(function () {
  modal.style.display = "none";
  body.style.overflow = 'visible';
})

// })

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
    body.style.overflow = 'visible';
  }
}

/* Modal form focus*/
let modalInputs = $('.modal-inputs input');
// Same as click
modalInputs.focus(function () {
  modalInputs.each(function () {
    if ($(this).is(":focus")) {
      $(this).addClass('focus')
    } else {
      $(this).removeClass('focus')
    }
  })
})


modalInputs.blur(function () {
  if (!$(this).val()) {
    $(this).addClass('empty')
  } else {
    $(this).removeClass('empty')
  }
})

/* Modal input check */
$('#reservation').click(function () {
  let alert = $('.modal-alert')
  let empty = false;
  modalInputs.each(function() {
    if (!$(this).val()) {
      empty = true;
      $(this).addClass('empty')
    } else {
      $(this).removeClass('empty')
    }
    $(this).removeClass('focus')
  })
  if (empty) {
    alert.css('display', 'block')
    //alert('Červené udaje byly vyplněny špatně! Opravte je a stiskněte tlačítko REZERVOVAT znovu')
  } else {
    alert.css('display', 'none')
  }
})

/* Photos */
$('.photos img').click(function () {
  let photoSrc = $(this).attr('src');
  $('body').css('overflow', 'hidden')
  $(modal).css('display', 'block')
  $(modalForm).css('display', 'none')
  //$(modalPhoto).css('display', 'block')
  $(modalPhoto).css('display', 'block')
  $(modalPhoto).attr('src', photoSrc)
  // $('#opened-photo').css('display', 'block')
  // $('#opened-photo').attr('src', photoSrc)
})


/* Arrow */
$('.arrow').click(function () {
  $('html, body').animate({
    scrollTop: 0
  }, 1000)
})

$(window).scroll(function () {
  if ($(this).scrollTop() > 800) {
    $('.arrow').fadeIn();
  } else {
    $('.arrow').fadeOut();
  }
});

// modalInputs.forEach(input => {
//   input.addEventListener('click', (event) => {
//     console.log(this === document.activeElement)
//   })
// })

/*
$(window).scroll(function() {
 var targetScroll1 = $('#section1').position().top;
 var targetScroll2 = $('#section2').position().top;
 var targetScroll3 = $('#section3').position().top;
 var targetScroll4 = $('#section4').position().top;
 var fromTop = $(window).scrollTop();


 console.log("from top " + fromTop);
 console.log("target scroll " + targetScroll2);
 console.log($('#section2').innerHeight());


 if(fromTop > targetScroll1) {
   $("a[href='#section1']").addClass('gold-scroll')
   $("a[href='#section2']").removeClass('gold-scroll')
 } else if (fromTop > targetScroll2) {
   $("a[href='#section1']").removeClass('gold-scroll')
   $("a[href='#section2']").addClass('gold-scroll')
   $("a[href='#section3']").removeClass('gold-scroll')
 } else if (fromTop > targetScroll3) {
   $("a[href='#section2']").removeClass('gold-scroll')
   $("a[href='#section3']").addClass('gold-scroll')
   $("a[href='#section4']").removeClass('gold-scroll')
 } else if (fromTop > targetScroll4) {
   $("a[href='#section3']").removeClass('gold-scroll')
   $("a[href='#section4']").addClass('gold-scroll')
 }
});
*/

/*
$(window).scroll(function() {
  var hT = $('#section1').offset().top,
      hH = $('#section1').outerHeight(),
      wH = $(window).height(),
      wS = $(this).scrollTop();
  if (wS > (hT+hH-wH) && (hT > wS) && (wS+wH > hT+hH)){
    console.log($(this))
    console.log('cau')
  } else {
    $(this).removeClass('gold-scroll')
  }
}); */


/*
$.fn.isOnScreen = function(){
  var win = $(window);

  var viewport = {
      top : win.scrollTop(),
      left : win.scrollLeft()
  };
  viewport.right = viewport.left + win.width()/4;
  viewport.bottom = viewport.top + win.height()/4;

  var bounds = this.offset();
  bounds.right = bounds.left + this.outerWidth();
  bounds.bottom = bounds.top + this.outerHeight();

  return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
};

$(window).scroll(function() {
  if ($('#section1').isOnScreen() == true) {
    //alert("removing orange");
    console.log('section1')
    $(this).addClass('gold-scroll')
  } else {
    $(this).removeClass('gold-scroll')
  }

  if ($('#section2').isOnScreen() == true) {
    //alert("removing orange");
    $(this).addClass('gold-scroll')
  } else {
    $(this).removeClass('gold-scroll')
  }
}); */

// npx serve



      // if (docSnap.exists()) {
      //   var time1 = docSnap.get("10");
      //   var time2 = docSnap.get("12");

      //   console.log("Document data:", docSnap.get("10"));
      //   console.log("Document data:", docSnap.get("12"));

      //   if (time1 == false) {
      //     $('#but1').removeClass('button');
      //     $('#but1').addClass('button-red');
      //     $('#but1').prop("disabled", true);
      //   } else {
      //     $('#but1').removeClass('button-red');
      //     $('#but1').addClass('button');
      //     $('#but1').prop("disabled", false);
      //   }

      //   if (time2 == false) {
      //     $('#but2').removeClass('button');
      //     $('#but2').addClass('button-red');
      //     $('#but2').prop("disabled", true);
      //   } else {
      //     $('#but2').removeClass('button-red');
      //     $('#but2').addClass('button');
      //     $('#but2').prop("disabled", false);
      //   }
      // } else {
      //   // doc.data() will be undefined in this case
      //   $('#timebuttons').children('button').each(function () {
      //     if ($(this).hasClass('button-red')) {
      //       $(this).removeClass('button-red')
      //     }

      //     if (!$(this).hasClass('button')) {
      //       $(this).addClass('button');
      //       $(this).prop("disabled", false);
      //     }
      //   });
      // }

//$("#change_color").css("background", "green");

//$( "#datepicker" ).datepicker( $.datepicker.regional[ "cs" ] );

// document.getElementById("myBtn").addEventListener('click', e => {
//     //e.preventDefault();

//     document.getElementById("demo").innerHTML = "prdka vole";
// });

// document.getElementById("myBtn").addEventListener("click", function() {
//     document.getElementById("demo").innerHTML = "Hello World";
//   });


// const el = document.getElementById("myBtn");
// el.addEventListener("click", buttonClick, false);

// async function buttonClick() {
//   try {
//     const docRef = await addDoc(collection(db, "users"), {
//       first: "Ada",
//       last: "Lovelace",
//       born: 1815
//     });
//     console.log("Document written with ID: ", docRef.id);
//   } catch (e) {
//     console.error("Error adding document: ", e);
//   }
// }

// ctrl + D  same word
// alt + click multiple cursors
// alt + up / down to move line
// alt + shift + up / down to move a copy line
// ctrl + L highlight lines
// ctlr + ` to  terminal